import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AgoravideocallMac {
  static const MethodChannel _channel =
      const MethodChannel('agoravideocall_mac');
  static final BasicMessageChannel _eventChannel = const BasicMessageChannel(
      'agora_rtc_engine_message_channel', StandardMessageCodec());
  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> agora() async {
    final String version = await _channel.invokeMethod('agorTest', {
      "appId": "9d73c92b459b4cbaad3f73ea099961c4",
      "token":
          "0069d73c92b459b4cbaad3f73ea099961c4IABDpu01OymyKiq6NWr4R+gEHB/va3INK/RAjFyRZTZ2TC0uLpYAAAAAEAA+AzVUg3RlYAEAAQCCdGVg"
    });
    _addEventChannelHandler();
    return version;
  }

  static Future<String> test() async {
    final String version = await _channel.invokeMethod(
      'test',
    );
    return version;
  }

  static Future<bool> leaveChannel() async {
    final bool success = await _channel.invokeMethod('leaveChannel');
    return success;
  }

  static StreamSubscription<dynamic> _sink;

  static StreamController<dynamic> _sinkController =
      StreamController<dynamic>.broadcast();
  static void Function(dynamic err) onError;
  static void init() {
    _eventChannel.setMessageHandler((message) {
      _sinkController.add(message);
    });
  }

  static void _addEventChannelHandler() async {
    _sink = _sinkController.stream.listen(_eventListener, onError: onError);
  }

  static void _removeEventChannelHandler() async {
    await _sink.cancel();
  }

  static void Function(String channel, int uid, int elapsed)
      onJoinChannelSuccess;
  // CallHandler
  static void _eventListener(dynamic event) {
    final Map<dynamic, dynamic> map = event;
    print(map['event']);
    switch (map['event']) {
      case 'onJoinChannelSuccess':
        if (onJoinChannelSuccess != null) {
          onJoinChannelSuccess(map['channel'], map['uid'], map['elapsed']);
        }
        break;
      case 'onLeaveChannel':
        break;
      case 'onUserJoined':
        break;
      case 'onUserOffline':
        break;
      case 'onRtcStats':
        break;
      case 'onRemoteAudioStats':
        break;
    }
  }
}
