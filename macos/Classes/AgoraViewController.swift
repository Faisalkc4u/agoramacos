//
//  AgoraViewController.swift
//  Runner
//
//  Created by mac on 5/4/21.
//

import Cocoa

class AgoraViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    class func initViewController() -> AgoraViewController
    {
        let storyboard = NSStoryboard(name: "Agora", bundle: nil)
        return storyboard.instantiateController(withIdentifier: String(describing: self)) as! AgoraViewController
    }
}
