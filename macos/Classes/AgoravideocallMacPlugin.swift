import Cocoa
import FlutterMacOS
import AgoraRtcKit
import AGEVideoLayout

public class AgoravideocallMacPlugin: NSObject,FlutterPlugin,NSApplicationDelegate {
    var localView: NSView!
    var agoraKit: AgoraRtcEngineKit!
    private var agoraRtcEngine: AgoraRtcEngineKit?
 
private var messageChannel: FlutterBasicMessageChannel!
  public static func register(with registrar: FlutterPluginRegistrar) {
    let instance = AgoravideocallMacPlugin()
    let channel = FlutterMethodChannel(name: "agoravideocall_mac", binaryMessenger: registrar.messenger)
    
    instance.messageChannel = FlutterBasicMessageChannel(name: "agora_rtc_engine_message_channel", binaryMessenger: registrar.messenger)
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
      let method = call.method
    let params = call.arguments as? Dictionary<String, Any>
    print("plugin handleMethodCall: \(method), args: \(String(describing: params))")
    switch call.method {
      case "test":
openAgora()
    case "getPlatformVersion":
      result("macOS " + ProcessInfo.processInfo.operatingSystemVersionString)
      case "agorTest":
      let appId = params?["appId"] as! String
      let token = params?["token"] as! String
       let config = AgoraRtcEngineConfig()
        config.appId = appId
        config.areaCode = GlobalSettings.shared.area.rawValue
       agoraKit = AgoraRtcEngineKit.sharedEngine(with: config, delegate: self)
      agorTest(token:token)
    default:
      result(FlutterMethodNotImplemented)
    }
  }

  func agorTest(token: String){
  agoraKit?.enableVideo()

  let videoCanvas = AgoraRtcVideoCanvas()
  videoCanvas.uid = 0
  videoCanvas.renderMode = .hidden
  videoCanvas.view = nil
  // Sets the local video view
    agoraKit?.setupLocalVideo(videoCanvas)
    


    // Joining a channel
    agoraKit?.joinChannel(byToken: token, channelId: "mvp", info: nil, uid: 0, joinSuccess: { (channel, uid, elapsed) in
        print( "Join \(channel) with uid \(uid) elapsed \(elapsed)ms")

      })
  }
  func openAgora() {
   let agoraView = AgoraViewController.initViewController()
//    MainFlutterWindow.flutterView.presentAsSheet(agoraView)
//    self.window?.contentViewController?.presentAsSheet(agoraView)
//    FlutterViewController.presentAsSheet(agoraView)
    
  }
 private func sendEvent(_ name: String, params: Dictionary<String, Any>) {
    var p = params
    p["event"] = name
    messageChannel.sendMessage(p)
  }
  
  func leaveChannel()  {
    agoraKit?.leaveChannel(nil)

  }
  
  func dispose()
  {
    AgoraRtcEngineKit.destroy()

  }

}




extension AgoravideocallMacPlugin: AgoraRtcEngineDelegate {
  public func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
      print("onfirstremote swift");
        sendEvent("onfirstremote", params: [ "uid": uid, "elapsed": elapsed])
    }
  public func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
    sendEvent("onJoinChannelSuccess", params: ["channel": channel, "uid": uid, "elapsed": elapsed])
  }

  public func rtcEngine(_ engine: AgoraRtcEngineKit, didLeaveChannelWith stats: AgoraChannelStats) {
    sendEvent("onLeaveChannel", params: ["stats": stats.toDictionary()])
  }

  public func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
    sendEvent("onUserJoined", params: ["uid": uid, "elapsed": elapsed])
  }

  public func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
    sendEvent("onUserOffline", params: ["uid": uid, "reason": reason.rawValue])
  }

  public func rtcEngine(_ engine: AgoraRtcEngineKit, reportRtcStats stats: AgoraChannelStats) {
    sendEvent("onRtcStats", params: ["stats": stats.toDictionary()])
  }

  public func rtcEngine(_ engine: AgoraRtcEngineKit, remoteAudioStats stats: AgoraRtcRemoteAudioStats) {
    sendEvent("onRemoteAudioStats", params: [
      "stats": [
        "uid": stats.uid,
        "quality": stats.quality,
        "networkTransportDelay": stats.networkTransportDelay,
        "jitterBufferDelay": stats.jitterBufferDelay,
        "audioLossRate": stats.audioLossRate,
        "numChannels": stats.numChannels,
        "receivedSampleRate": stats.receivedSampleRate,
        "receivedBitrate": stats.receivedBitrate,
        "totalFrozenTime": stats.totalFrozenTime,
        "frozenRate": stats.frozenRate,
      ]
    ])
  }
}

extension AgoraChannelStats {
  func toDictionary() -> [String: Any] {
    [
      "totalDuration": duration,
      "txBytes": txBytes,
      "rxBytes": rxBytes,
      "txAudioBytes": txAudioBytes,
      "txVideoBytes": txVideoBytes,
      "rxAudioBytes": rxAudioBytes,
      "rxVideoBytes": rxVideoBytes,
      "txKBitrate": txKBitrate,
      "rxKBitrate": rxKBitrate,
      "txAudioKBitrate": txAudioKBitrate,
      "rxAudioKBitrate": rxAudioKBitrate,
      "txVideoKBitrate": txVideoKBitrate,
      "rxVideoKBitrate": rxVideoKBitrate,
      "lastmileDelay": lastmileDelay,
      "txPacketLossRate": txPacketLossRate,
      "rxPacketLossRate": rxPacketLossRate,
      "users": userCount,
      "cpuAppUsage": cpuAppUsage,
      "cpuTotalUsage": cpuTotalUsage,
    ]
  }
}





